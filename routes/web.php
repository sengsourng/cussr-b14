<?php

use App\Http\Controllers\Admin\ManageUserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Admin\TodoController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home',function(){
    // return "Hello Test";
    return view('front.home');
});

Route::get('/about',function(){
    // return "Hello Test";
    return view('front.about');
});

// Route::get('/students',[StudentController::class,'index'])->name('students.index');
// Route::resource('categories', CategoryController::class);



Auth::routes();


Route::group(['prefix' => 'admin','middleware' => 'auth'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('/hello', function() {
        return "Hello Dashboard";
    } );

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
    Route::resource('/todos',TodoController::class);
    Route::resource('/students',StudentController::class);
    Route::resource('/classes',ClassController::class);

    // Users
    Route::resource('/users', ManageUserController::class);


});
















// Route::group(['prefix' => 'admin', 'as' => 'admin.','middleware' => ['web', 'auth','isAdmin']], function () {
//     // Route::get('/', [DashboardController::class, 'Dashboard'])->name('dashboard');
//     Route::resources([
//         'classes' => 'App\Http\Controllers\Admin\ClassesController',
//         // 'post' => 'App\Http\Controllers\Admin\PostController',
//         // 'tag' => 'App\Http\Controllers\Admin\TagController',
//         // 'user' => 'App\Http\Controllers\Admin\UserController',
//         // 'comment' => 'App\Http\Controllers\Admin\CommentController'
//     ]);
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
