<?php

namespace App\Http\Controllers\Admin;

use App\Models\Todo;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=[];
        $data['Todos']=Todo::get();

        return view('admin.todos.index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data=[];
        $data['classes']=Classes::get();
        return view('admin.todos.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        // dd($request);
        foreach($request->input('title') as $key => $value) {
            $data["title.{$key}"] = 'required';
        }
        // foreach($request->input('category_id') as $key => $cat) {
        //     $data["category_id.{$key}"] = 'required';
        // }
        $cat=$request->category_id;




        $validator = Validator::make($request->all(), $data);

        if ($validator->passes()) {

            // foreach($request->input('title') as $key => $value) {
            //     Todo::create([
            //         'title'=>$value,
            //         'category_id' => 1,
            //         'user_id'   =>Auth::user()->id
            //     ]);
            // }

            foreach($request->input('title') as $key => $value) {
                Todo::create([
                    'title'=>$value,
                    'category_id' => $cat[$key],
                    'user_id'   =>Auth::user()->id
                ]);
            }

            return response()->json(['success'=>'true']);

        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Todo::find($id)->delete($id);

        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }
}
