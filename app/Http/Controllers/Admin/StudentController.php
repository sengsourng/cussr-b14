<?php

namespace App\Http\Controllers\Admin;

use App\Models\Classes;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data=[];
        $data['students']=Student::get();

        // return $data['students'];
        return view('admin.students.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[];
        $data['classes']=Classes::get();

        return view('admin.students.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        Student::create([
            'name' =>$request->name,
            'sex'  =>$request->sex,
            'dob'   =>$request->dob,
            'address' =>$request->address,
            'status'    => $request->status,
            'class_id'  =>$request->class_id,
            'user_id'   => Auth::user()->id
        ]);

        return redirect('admin/students')->with('success',"Save Student success!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $data=[];
       $data['student']=Student::findOrFail($id);

       return view('admin.students.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[];

        $data['classes']=Classes::get();
       $data['student']=Student::findOrFail($id);

       return view('admin.students.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;

        $student=Student::findOrFail($id);
        $student->update([
            'name'  =>$request->name,
            'sex'   =>$request->sex,
            'status' =>$request->status,
        ]);

        return redirect('admin/students');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Student::find($id)->delete($id);

        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);

    }
}
