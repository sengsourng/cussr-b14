<?php

namespace App\Http\Controllers;

use App\Models\Classes;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function index()
    {
        $data=[];
        $data['classes']=Classes::get();

        // return $data['classes'];
        return view('admin.classes.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // // return "Hello Create";
        // Classes::create([
        //     'name'      =>'Grade 05',
        //     'sort'      =>'5',
        //     'status'    =>  0,
        // ]);
        // // return "Create success!";
        // return redirect('/classes');

        return view('admin.classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        Classes::create([
            'name'      =>$request->name,
            'sort'      =>$request->sort,
            'status'    => $request->status,
        ]);

        return redirect('/admin/classes');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return "Hello".$id;
        $data=[];
        $data['class']=Classes::findOrFail($id);
        // return($data['ShowClass']);
        return view('admin.classes.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[];
        $data['class']=Classes::findOrFail($id);
        // return($data['ShowClass']);
        return view('admin.classes.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;
        $class=Classes::findOrFail($id);

        $class->update([
            'name'  =>  $request->name,
            'sort'  =>  $request->sort,
            'status'=>  $request->status,

        ]);

        return redirect('/classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $class = Classes::findOrFail($id);

        // if ($class->delete()) {
        //     $class->tags()->detach();
        // }
        // return redirect()->route('classes.index');
            Classes::find($id)->delete($id);

        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }
}
