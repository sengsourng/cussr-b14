<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('titel','Home Page')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('includes.style')
    @stack('css')
</head>
<body>
@include('includes.nav')

{{-- @include('front.home') --}}
@yield('content')

@include('includes.footer')

@stack('js')
</body>
</html>

