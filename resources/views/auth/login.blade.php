@extends('layouts.app')

@section('content')

<div class="container login-container">
    <div class="row">
        <div class="col-md-6 login-form-1">
            <h3 style="font-family: 'Khmer OS Battambang'; ">ចូលប្រើ​ប្រព័ន្ធ</h3>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    {{-- <input type="text" class="form-control" placeholder="Your Email *" value="" /> --}}


                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Your Email *">

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror


                </div>
                <div class="form-group">
                    {{-- <input type="password" class="form-control" placeholder="Your Password *" value="" /> --}}
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Your Password *">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    <div class="col-md-6 ">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <input type="submit" class="btnSubmit" value="Login" />
                </div>
                <div class="form-group">
                    {{-- <a href="#" class="ForgetPwd">Forget Password?</a> --}}
                    @if (Route::has('password.request'))
                    <a class="ForgetPwd" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif
                </div>
            </form>
        </div>
        <div class="col-md-6 login-form-2" style="background-image:url('{{asset('uploads/bg.png')}}')">
            {{-- <h3>School Managment</h3> --}}
            {{-- <img style="height: 500px;" src="{{asset('uploads/bg.png')}}" alt=""> --}}

        </div>
    </div>
</div>





@endsection
