@extends('layouts.admin')
@section('title',"List User")

@push('css')
    {{-- Data Table Style --}}
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('content')

    <div class="card">
        <div class="card-header">
        <h3 class="card-title">User List</h3> <span><a class="btn btn-primary btn-sm float-right" href="#"> Add New</a></span>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>User Type</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>

                @foreach ($Users as $row)
                    <tr>
                        <td>
                            {{$row->id}}
                        </td>
                        <td>
                            @php
                                $profile=$row->photo!=NULL?$row->photo : 'users/default.jpg';
                            @endphp
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm" src="{{asset('uploads/'. $profile )}}" alt="User Image">
                                <span class="username">
                                <a href="{{url('admin/user_profile/'.$row->id)}}">{{$row->name}}</a>
                                {{-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> --}}
                                </span>
                                <span class="description">3 days ago</span>
                            </div>
                        </td>
                        <td>
                            {{$row->email}}
                        </td>

                        <td>
                            <span class="btn btn-{{$row->is_active=='1'?'success':'danger'}} btn-sm">{{$row->user_type}}</span>
                        </td>
                        <td>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">


                                <a class="btn btn-primary btn-sm active" href="{{url('admin/users/'.$row->id)}}"> View</a>

                                <a class="btn btn-success btn-sm active" href="{{url('admin/users/'. $row->id .'/edit')}}"> Edit</a>


                                <label class="btn btn-danger btn-sm">
                                <input type="radio" name="options" id="option3" autocomplete="off"> Delete
                                </label>
                            </div>

                        </td>
                </tr>
                @endforeach

            </tbody>
            <tfoot>
            <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>User Type</th>
            <th>Action</th>
            </tr>
            </tfoot>
        </table>
        </div>
        <!-- /.card-body -->
    </div>

@endsection

@push('js')
  <!-- DataTables -->
    <script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('backend/dist/js/demo.js')}}"></script>
    <!-- page script -->
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
        });
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>

@endpush
