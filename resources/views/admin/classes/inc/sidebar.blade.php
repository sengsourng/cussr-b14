<div class="col-md-3">
    <div class="list-group">
        <a href="{{url('/home')}}" class="list-group-item list-group-item-action">Home</a>
        <a href="{{url('/admin/students')}}" class="list-group-item list-group-item-action">Students</a>
        <a href="{{url('/admin/classes')}}" class="list-group-item list-group-item-action">Classes</a>

        <a href="{{url('/admin/todos')}}" class="list-group-item list-group-item-action">Todos</a>

    </div>
</div>
