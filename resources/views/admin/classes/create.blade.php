@extends('layouts.admin')
@section('title','Create Class')
@push('css')

@endpush
@section('content')
            <form method="POST" action="{{route('classes.store')}}">
                @csrf
                <div class="card">
                    <div class="card-header"><h4>{{'Create Class'}} <span class="float-right"></h4> </div>
                            <div class="container">

                                <div class="form-group">
                                    <label for="name">Class Name:</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Class Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="sort">Sort:</label>
                                    <input type="text" class="form-control" id="sort" name="sort" placeholder="Enter Sort" required>
                                </div>
                                <div class="form-group">
                                    <label for="sel1">Status:</label>
                                    <select class="form-control" id="sel1" name="status">
                                    <option value="1">Active</option>
                                    <option value="0">Not Active</option>

                                    </select>
                                </div>

                            </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-danger float-right" href="{{url('/admin/classes')}}">Cancel</a>
                   </div>
               </div>

            </form>
@endsection

@push('js')

<script type="text/javascript">

</script>
@endpush
