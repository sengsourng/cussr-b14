@extends('layouts.app')
@section('title','About Page')
@push('css')

@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        @include('admin.classes.inc.sidebar')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header"><h4>{{'Class Name'}} <span class="float-right"><a class="btn btn-primary" href="{{url('classes/create')}}">Create</a></span></h4>
                </div>
                <div class="card-body">
                    <div class="container">

                        {{-- Your form here --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection









@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
//    $(".deleteRecord").click(function(){
//     //    alert("Hello");
//     var id = $(this).data("id");
//     var token = $("meta[name='csrf-token']").attr("content");

//     swal({
//         title: "Are you sure?",
//         text: "Please Check before Delete!",
//         icon: "warning",
//         buttons: true,
//         dangerMode: true,
//         })
//         .then((willDelete) => {
//         if (willDelete) {
//             $.ajax(
//                     {
//                          url: "classes/"+id,
//                         type: 'DELETE',
//                         data: {
//                             "id": id,
//                             "_token": token,
//                         },
//                         success: function (){
//                             swal("Poof! Your record has been deleted!", {
//                             icon: "success",
//                             });
//                             window.location.href = "{{url('/classes')}}";
//                         }
//                     });
//         } else {
//             swal("Your record is safe!");
//         }
//         });
// });
</script>
@endpush
