@extends('layouts.admin')
@section('title','List Student')

@push('css')

@endpush

@section('content')
    <div class="card">
        <div class="card-header"><h4>{{'Student List'}} <span class="float-right"><a class="btn btn-primary" href="{{url('admin/students/create')}}">Create</a></span></h4>
        </div>
            <table class="table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Student Name</th>
                      <th>Created at</th>
                      <th>Option</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($students as $item)
                      <?php $id=($item->id%2); ?>
                      <tr class="{{$id==0?'success':''}}" >
                          <td>{{$item->id}}</td>
                          <td>
                              @if ($item->status ==1)
                                  <i class="fa fa-circle text-success"></i>
                              @else
                                  <i class="fa fa-circle text-danger"></i>
                              @endif
                              {{$item->name}}
                          </td>
                          <td>{{$item->created_at}}</td>
                          <td>
                              <a href="{{ route('students.show',$item->id) }}" class="btn btn-warning btn-sm">
                                  Show
                              </a>
                              <a href="{{ route('students.edit',$item->id) }}" class="btn btn-success btn-sm">Edit</a>
                              <button class="deleteRecord btn btn-danger btn-sm" data-id="{{ $item->id }}" >Delete</button>
                          </td>
                      </tr>
                    @endforeach

                  </tbody>
            </table>

    </div>

@endsection

@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
   $(".deleteRecord").click(function(){
    //    alert("Hello");
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");

    swal({
        title: "Are you sure?",
        text: "Please Check before Delete!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax(
                    {
                         url: "students/"+id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (){
                            swal("Poof! Your record has been deleted!", {
                            icon: "success",
                            });
                            window.location.href = "{{url('/admin/students')}}";
                        }
                    });
        } else {
            swal("Your record is safe!");
        }
        });
});
</script>
@endpush
