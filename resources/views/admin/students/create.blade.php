@extends('layouts.admin')
@section('title','Create Class')
@push('css')

@endpush
@section('content')
            <form method="POST" action="{{route('students.store')}}">
                @csrf
                <div class="card">
                    <div class="card-header"><h4>{{'Create Student'}} <span class="float-right"></h4> </div>
                            <div class="container">

                               <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name">Student Name:</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Class Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="sel1">Gender:</label>
                                            <select class="form-control" id="sel1" name="sex">
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="dob">Date of Birth:</label>
                                            <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter Sort" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Class:</label>
                                            <select class="form-control" id="sel1" name="class_id">
                                                @foreach ($classes as $class)
                                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Address:</label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address ex.Siem Reap" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Status:</label>
                                            <select class="form-control" id="sel1" name="status">
                                                <option value="1">Active</option>
                                                <option value="0">Not Active</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            {{-- {!! Form::file($name, [$options]) !!} --}}
                                            <input type="file" class="btn btn-primary" name="profile" id="" style="width: 100%;">
                                            {{-- <button type="submit" class="btn btn-primary">Brow</button> --}}
                                    </div>
                               </div>

                            </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a class="btn btn-danger float-right" href="{{url('/admin/classes')}}">Cancel</a>
                   </div>
               </div>

            </form>
@endsection

@push('js')

<script type="text/javascript">

</script>
@endpush
