@extends('layouts.admin')
@section('title','Create Class')
@push('css')

@endpush
@section('content')
            <form method="POST" action="{{route('students.store')}}">
                {{-- @csrf
                {{ method_field('PUT') }} --}}
                <div class="card">
                    <div class="card-header"><h4>{{'Show Student'}} <span class="float-right"></h4> </div>
                            <div class="container">

                                <div class="form-group">
                                    <label for="name">Student Name:</label>
                                    <input type="text" disabled class="form-control" id="name" name="name" value="{{$student->name}}" placeholder="Enter Class Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="sort">Sort:</label>
                                    <input type="text" disabled class="form-control" id="sort" name="sort" value="{{$student->sex}}" placeholder="Enter Sort" required>
                                </div>
                                <div class="form-group">
                                    <label for="sel1">Status:</label>
                                    <select disabled class="form-control" id="sel1" name="status">
                                      <option {{$student->status==1?'selected':''}} value="1">Active</option>
                                      <option {{$student->status==0?'selected':''}} value="0">Not Active</option>

                                    </select>
                                </div>

                            </div>

                    <div class="card-footer">
                        {{-- <button type="submit" class="btn btn-primary">Update</button> --}}
                        <a class="btn btn-danger float-right" href="{{url('/admin/students')}}">Cancel</a>
                   </div>
               </div>

            </form>
@endsection

@push('js')

<script type="text/javascript">

</script>
@endpush
