@extends('layouts.admin')
@section('title','Create Class')
@push('css')

@endpush
@section('content')
            <form method="POST" action="{{route('students.update',$student->id)}}" enctype="multipart/form-data">

                @csrf
                @method('PUT')

                <div class="card">
                    <div class="card-header"><h4>{{'Create Student'}} <span class="float-right"></h4> </div>
                            <div class="container">

                               <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name">Student Name:</label>
                                            <input type="text" class="form-control" id="name" name="name" value="{{$student->name}}" placeholder="Enter Class Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="sel1">Gender:</label>
                                            <select class="form-control" id="sel1" name="sex">
                                                <option {{$student=='Male'?'selected':''}} value="Male">Male</option>
                                                <option {{$student=='Female'?'selected':''}} value="Female">Female</option>

                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="dob">Date of Birth:</label>
                                            <input type="date" class="form-control" id="dob" name="dob" value="{{$student->dob}}" placeholder="Enter Sort" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Class:</label>
                                            <select class="form-control" id="sel1" name="class_id">
                                                @foreach ($classes as $class)
                                                    <option {{$class->id==$student->class_id?'selected':''}} value="{{$class->id}}">{{$class->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Address:</label>
                                            <input type="text" class="form-control" id="address" name="address" value="{{$student->address}}" placeholder="Enter Address ex.Siem Reap" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="sel1">Status:</label>
                                            <select class="form-control" id="sel1" name="status">
                                                <option {{$student->status==1?'selected':''}} value="1">Active</option>
                                                <option {{$student->status==0?'selected':''}} value="0">Not Active</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                            {{-- {!! Form::file($name, [$options]) !!} --}}
                                            <input type="file" class="btn btn-primary" name="profile" id="" style="width: 100%;">
                                            {{-- <button type="submit" class="btn btn-primary">Brow</button> --}}
                                    </div>
                               </div>

                            </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a class="btn btn-danger float-right" href="{{url('/admin/classes')}}">Cancel</a>
                   </div>
               </div>

            </form>
@endsection

@push('js')

<script type="text/javascript">

</script>
@endpush
