@extends('layouts.admin')
@section('title','List Classes')

@push('css')

@endpush

@section('content')
    <div class="card">
        <div class="card-header"><h4>{{'Todo List'}} <span class="float-right"><a class="btn btn-primary" href="{{url('admin/todos/create')}}">Create</a></span></h4>
        </div>
            <table class="table">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Todo Title</th>
                      <th>Created at</th>
                      <th>Option</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($Todos as $item)
                      <?php $id=($item->id%2); ?>
                      <tr class="{{$id==0?'success':''}}" >
                          <td>{{$item->id}}</td>
                          <td>
                              @if ($item->status ==1)
                                  <i class="fa fa-circle text-success"></i>
                              @else
                                  <i class="fa fa-circle text-danger"></i>
                              @endif
                              {{$item->title}}
                          </td>
                          <td>{{$item->created_at}}</td>
                          <td>
                              <a href="{{ route('todos.show',$item->id) }}" class="btn btn-warning btn-sm">
                                  Show
                              </a>
                              <a href="{{ route('todos.edit',$item->id) }}" class="btn btn-success btn-sm">Edit</a>
                              <button class="deleteRecord btn btn-danger btn-sm" data-id="{{ $item->id }}" >Delete</button>
                          </td>
                      </tr>
                    @endforeach

                  </tbody>
            </table>

    </div>

@endsection

@push('js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
   $(".deleteRecord").click(function(){
    //    alert("Hello");
    var id = $(this).data("id");
    var token = $("meta[name='csrf-token']").attr("content");
    // Swal.fire({
    //         title: 'Are you sure?',
    //         text: "You won't be able to revert this!",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, delete it!'
    //         }).then((result) => {
    //         if (result.isConfirmed) {
    //             $.ajax(
    //                 {
    //                      url: "todos/"+id,
    //                     type: 'DELETE',
    //                     data: {
    //                         "id": id,
    //                         "_token": token,
    //                     },
    //                     success: function (){
    //                         swal("Poof! Your record has been deleted!", {
    //                         icon: "success",
    //                         });
    //                         window.location.href = "{{url('/admin/todos')}}";
    //                     }
    //                 });
    //             Swal.fire(
    //             'Deleted!',
    //             'Your file has been deleted.',
    //             'success'
    //             )
    //         }
    //         });


    swal({
        title: "Do you want to delete this?",
        text: "Please Check before Delete!",
        icon: "warning",
        confirmButtonColor: '#3085d6',
        buttons: true,
        dangerMode: true,

        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax(
                    {
                         url: "todos/"+id,
                        type: 'DELETE',
                        data: {
                            "id": id,
                            "_token": token,
                        },
                        success: function (){
                            swal("Poof! Your record has been deleted!", {
                            icon: "success",
                            });
                            window.location.href = "{{url('/admin/todos')}}";
                        }
                    });
        }
        //  else {
        //     swal("Your record is safe!");
        // }
        });
});
</script>
@endpush
