@extends('layout.master')
@section('title','About Page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3>Left Side bar</h3>
                <img style="width: 100%;" src="{{asset('image/bootstrap4.png')}}" alt="">
            </div>
            <div class="col-md-8">
                <h1>About</h1>
    <p>
        Images come in all sizes. So do screens. Responsive images automatically adjust to fit the size of the screen.

Create responsive images by adding an .img-fluid class to the <img> tag. The image will then scale nicely to the parent element.

The .img-fluid class applies max-width: 100%; and height: auto; to the image:
    </p>
            </div>
        </div>
    </div>
@endsection
