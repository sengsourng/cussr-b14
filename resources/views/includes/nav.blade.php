<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <ul class="navbar-nav">
        <li class="nav-item">
        <a class="nav-link" href="{{url('/')}}">Training ICT CUS B14</a>
        </li>
        <li class="nav-item {{ Request::is('home')?'active':''}}">
        <a class="nav-link" href="{{url('/home')}}">Home</a>
        </li>
        <li class="nav-item {{ Request::is('about')?'active':''}}">
        <a class="nav-link" href="{{url('/about')}}">About</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Student</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Category</a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ Request::is('classes')?'active':''}}" href="{{url('/classes')}}">Classes</a>
        </li>

    </ul>
</nav>
