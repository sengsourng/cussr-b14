<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAttendantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attendants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('manage_classes_id')->nullable();
            $table->foreignId('classes_id')->nullable();
            $table->foreignId('teachers_id')->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->tinyInteger('status')->nullable(); // 1: Present, 0: Absense, 2:Late, 3:Permision

            $table->string('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attendants');
    }
}
