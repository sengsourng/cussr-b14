<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManageClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_classes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('teachers_id')->nullable();
            $table->foreignId('class_years_id')->nullable();
            $table->foreignId('classes_id')->nullable();

            $table->integer('score_divide')->nullable();

            $table->string('manage_class_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_classes');
    }
}
